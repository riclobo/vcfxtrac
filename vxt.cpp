//------------------------------------------------------------------------------
// Command line utility to extract information (email listings) from vCard files
// and also some other utilities on plain text files
//
// Use this code all you want but don't bother me with feature requests and 
// stuff. Also it may or may not work, so be careful. I am not really concerned 
// by error checking and similar things. 
//
// I have a tendency to overcomment my code. So if you don't understand what the
// code is doing you should probably consider not programming in c++. 
//
// I did not try to optimize this code. My approach was to get it in a working 
// state as fast as a could.
//
// My motivation to writing this was that I did not find any thing doing this 
// conversion already. I will probably use this code only once. This is a 
// typical task for python. But I know c++ better than python. So...
//
// If, despite all of the above, you still want to play with it, enjoy.
//
// R.P.S.M. Lobo
// 
// Usage:
// $ ./vxt [options] inputfile [> outputfile]
//
// Known commands:
//  -m : parses a vCard file and extracts a list of emails.
//  -u : parses a file and outputs a list of unique, sorted strings. Can be
//       combined with -m.
//  -v : verbose mode. Starts output with a short summary of actions.
//------------------------------------------------------------------------------
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
//------------------------------------------------------------------------------
using std::cout;
using std::endl;
using std::ifstream;
using std::string;
using std::vector;
using std::set;
//------------------------------------------------------------------------------
// Parse command line. On success, returns true. 'fn' will contain the input
// file name and 'c' the list of commands.
//------------------------------------------------------------------------------
bool ParseCL(int argc, char *argv[], string &fn, set<string> &c)
{  
  if (argc < 3)     // Needs at least one options and the filename
    return false;  // Return with an error
    
  string tmp;       // String to hold each item in argv			
  int items = 1;    // Counter to see how many items were already parsed
                    // Note that item 0 is the program name.
  do
  {
    tmp = argv[items++];  // Copy the first item to tmp and increment items. 
    if (tmp[0] == '-')    // First character is a '-' ...
      c.insert(tmp);      // ...put the string in the commands set.
    else                  // First character is not '-' ...
      fn = tmp;           // ...assume that this is the file name
  } while ((items < argc) && (tmp[0] == '-')); // Loops while there are items
                                                // left in argv or until item
                                                // in argv does not start with
                                                // a '-' (not a command).
  
  // Check whether the command list is empty                                               
  if (c.empty())  
  {
    cout << "No command was found." << endl;
    return false;
  }
  
  // Check if commands found are useful
  if ((c.find("-u") == c.end()) &&   // No "-u"...
      (c.find("-m") == c.end()))     // ...nor "-m" commands
  {
    cout << "At least one of '-u' or '-m' options must be present" << endl;
    return false;
  }
  
  return true;  // Success !
}
//------------------------------------------------------------------------------
// Get email from string (vCard format). Returns true if the string had email.
//------------------------------------------------------------------------------
bool IsvCardEmail(const string &s, string &mail)
{
  if (s.substr(0,5) != "EMAIL")    // 's' doesn't contain email...
    return false;                 // ...go back to main without further ado. 
    
  int i = s.find(":") + 1;  // Index of char just after ':' (where email begins)
  mail = s.substr(i);       // Email is 's' substring from i to the end
  return true;             // Tell main that all is swell.
}
//------------------------------------------------------------------------------
// Load file 'fn' into 'lines'. Return # lines read on success and -1 on fail.
// Read only emails if email == true.
//------------------------------------------------------------------------------
int LoadLines(const string &fn, vector<string> &lines, bool email)
{  
  ifstream filein(fn.c_str());   // Try to open file
  
  if (!filein)    // File not found or blocked or other problem
  {
    cout << "Cannot open " << fn << endl;  // Error message
    return -1;                            // Error code
  }

  // Read the file
  while (!filein.eof())
  {
    string line;      			// Space for each line in file
    getline(filein, line);      // Read one line

    if (email)     // Only keep lines that are emails in vCard format
    {
      string onemail;   // Space to hold email address
      if (IsvCardEmail(line, onemail))   // If the line is a vCard email...
        lines.push_back(onemail);        // ...puts it in the vector.
    }
    else           // Keep all lines
      lines.push_back(line);
  }
  
  return lines.size();
}    
//------------------------------------------------------------------------------
// Main function
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  string vcf;           // String to hold input file name
  vector<string> lines; // List to hold useful file output
  set<string> com;      // Commands (so far only -m and -u are valid)
  bool verbose,        // User wants extra info?  				
       email;           // User wants vCard emails only?
  
  // Tries to parse the command line
  if (!ParseCL(argc, argv, vcf, com))  // Failed
  {
	// Shows short help
    cout << endl << "Usage:" << endl
         << "  $ ./vxt [options] <inputfile> [> <outputfile>]" << endl << endl
         << "Options:" << endl
         << "  -m : parses a vCard file and extracts a list of emails." << endl
         << "  -u : parses a file and outputs a list of unique," << endl
         << "       sorted strings. Can be combined with -m." << endl
         << "  -v : starts output with a short summary of actions." << endl;
         
    return 1;  // Quit with an "error"
  }

  // Processing options
  verbose = (com.find("-v") != com.end());  // User wants extra info?
  email = (com.find("-m") != com.end());    // User wants vCard emails only?
  
  // Try to load vcf file. If -m command exists, loads only vCard emails.
  int nLines = LoadLines(vcf, lines, email);
  if (nLines < 0) return -1; // File opening failed. Quit.
  
  // Post processing and output
  if (verbose)
  {
    if (email) cout << "Extracted " << nLines << " emails from " << vcf << endl;
    else cout << "Read " << nLines << " lines from" << vcf << endl;
  }

  if (com.find("-u") != com.end())    // -u command - Keep unique strings only
  {
    sort(lines.begin(), lines.end());
    lines.erase( unique(lines.begin(), lines.end()), lines.end() );
    if (verbose) cout << "where " << lines.size() << " are unique." << endl; 
  }
     
  // Print lines
  for (vector<string>::iterator it = lines.begin(); it != lines.end(); it++)
    cout << *it << endl;
  
  return 0;   // Success
}

